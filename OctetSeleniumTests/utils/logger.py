import datetime

def console_log(msg):
    """Logs message to the console with current date time."""
    now = datetime.datetime.now()
    text = f"{now.year}-{now.month}-{now.day} {now.hour}:{now.minute}:{now.second}    {msg}"
    print("\n", text, "\n")
