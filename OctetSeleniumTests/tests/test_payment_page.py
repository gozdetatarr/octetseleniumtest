from page_objects.payment_page import PaymentPage

from selenium import webdriver

import unittest

class TestPaymentPage(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('./resources/chromedriver')

    def test_payment_with_given_amount(self):
        page = PaymentPage(self.driver)
        page.open("https://uat.octet.com.tr/Ode/ode.aspx?Code=/V9KP1/&c=TR")
        payment_status = page.pay()
        status_len = len(payment_status)
        #TODO: Convert Turkish character to English.
        self.assertEqual(status_len, 8)

    def tearDown(self):
        self.driver.quit()

