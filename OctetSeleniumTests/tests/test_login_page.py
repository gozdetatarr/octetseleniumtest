from page_objects.login_page import LoginPage
from utils.logger import console_log

from selenium import webdriver

import unittest

class TestLoginPage(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('./resources/chromedriver')

    def test_login_with_valid_user(self):
        page = LoginPage(self.driver)
        page.open("https://uat.octet.com.tr/")
        username = page.login().get_profile_username()
        self.assertEqual(username, "Serkan NAS")

    def tearDown(self):
        self.driver.quit()




