from page_objects.login_page import LoginPage 

from time import sleep 

webdriver.get_screenshots_as_file("OctetTestVisibleScreen")

webdriver.quit()

from selenium import webdriver
import unittest

class TestLoginPage(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('./resources/chromedriver')


    def test_login_with_valid_user(self):
        page = LoginPage(self.driver)
        page.open("https://uat.octet.com.tr/")
        username = page.login().get_profile_username()
        f= open ("newText.txt", "w+")
        f.write("Serkan Nas", username)
        f.close()

        self.assertEqual(username, "Serkan NAS")



    def tearDown(self):
        self.driver.quit()
