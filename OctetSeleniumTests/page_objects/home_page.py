from selenium.webdriver.common.by import By

from page_objects.base_page import BasePage

from utils.logger import console_log



class HomePage(BasePage):
    #Page Elements
    USERNAME_ELEM = (By.ID, "ctl00_LoginStatus_btnUserMenu")



    def __init__(self, driver):
        super().__init__(driver)



    def get_profile_username(self):
        """Returns username in the homepage."""
        return self.driver.find_element(*self.USERNAME_ELEM).text
