from selenium.webdriver.common.by import By

from page_objects.base_page import BasePage

from utils.logger import console_log
from time import sleep



class PaymentPage(BasePage):
    #Page Elements
    NEXT_BTN_ELEM = (By.ID, "Continue_Step1")
    PAYMENT_AMT_RADIO_BTN_ELEM = (By.ID, "payPartialOption")
    PAYMENT_AMT_INPUT_ELEM = (By.ID, "payPartialAmountInt")
    CARD_NAME_INPUT_ELEM = (By.ID, "cc_name")
    CARD_NO_INPUT_ELEM = (By.ID, "cc_number")
    CARD_END_MONTH_ELEM = (By.ID, "cc_expiry_ay")
    CARD_END_YEAR_ELEM = (By.ID, "cc_expiry_yil")
    CARD_SECURITY_CODE = (By.ID, "cc_cvc")
    AGREEMENT_CHECKBOX_ELEM = (By.ID, "accept_agreement")
    PAY_BTN_ELEM = (By.ID, "cmdPay")
    VERIFICATION_PASSWORD_ELEM = (By.NAME, "password")
    SUBMIT_BTN_ELEM = (By.NAME, "submit")
    PAYMENT_RESULT_ELEM = (By.ID, "StatusPaymentResult_TypeLabel")



    def __init__(self, driver):
        super().__init__(driver)
        self.url = "https://uat.octet.com.tr/Ode/ode.aspx?Code=/V9KP1/&c=TR"
        self.card_name = "card name"
        self.card_no = "4920244920244921"
        self.card_year = "2030"
        self.card_date = "12"
        self.card_security = "001"
        self.secure_payment_code = "a"



    def pay(self):
        "Performs payment operation. Returns payment status result."
        self.click_next_btn()
        self.enter_payment_amount()
        self.enter_card_info()
        self.accept_agreement()
        self.click_pay()
        self.submit_visa_verification()
        return self.get_payment_status_result()


        
    def scroll_to_element(self, elem):
        """Scrolls to the given element in the driver page."""
        self.driver.execute_script("arguments[0].scrollIntoView();", elem)


    
    def click_next_btn(self):
        """In the openning page of the payment page, clicks to the next btn."""
        next_btn_elem = self.driver.find_element(*self.NEXT_BTN_ELEM)
        self.driver.find_element(*self.NEXT_BTN_ELEM)
        self.scroll_to_element(next_btn_elem)
        next_btn_elem.click()
        sleep(3)
        self.driver.fullscreen_window()



    def enter_payment_amount(self):
        """Enters payment amount to the form."""
        payment_amt_radio_btn_elem = self.driver.find_element(*self.PAYMENT_AMT_RADIO_BTN_ELEM)
        payment_amt_radio_btn_elem.click()
        sleep(3)
        payment_amt_input_elem = self.driver.find_element(*self.PAYMENT_AMT_INPUT_ELEM)
        payment_amt_input_elem.send_keys("10")
        sleep(3)



    def enter_card_info(self):
        """Enters card info."""
        card_name_input_elem = self.driver.find_element(*self.CARD_NAME_INPUT_ELEM)
        card_no_input_elem = self.driver.find_element(*self.CARD_NO_INPUT_ELEM)
        card_end_month_elem = self.driver.find_element (*self.CARD_END_MONTH_ELEM)
        card_end_year_elem = self.driver.find_element (*self.CARD_END_YEAR_ELEM)
        card_security_code = self.driver.find_element(*self.CARD_SECURITY_CODE) 
        card_name_input_elem.send_keys(self.card_name)
        card_no_input_elem.send_keys(self.card_no)
        card_end_month_elem.send_keys(self.card_date)
        card_end_year_elem.send_keys(self.card_year)
        card_security_code.send_keys(self.card_security)
        sleep(2)


    
    def accept_agreement(self):
        """Selects checkbox to accept the agreement."""
        agreement_checkbox_elem = self.driver.find_element(*self.AGREEMENT_CHECKBOX_ELEM)
        self.scroll_to_element(agreement_checkbox_elem)
        agreement_checkbox_elem.click()
        sleep(2)



    def click_pay(self):
        """Clicks pay btn."""
        pay_btn_elem = self.driver.find_element(*self.PAY_BTN_ELEM)
        pay_btn_elem.click()
        sleep(15)



    def submit_visa_verification(self):
        """Sumbits visa verification form."""
        verification_password_elem = self.driver.find_element(*self.VERIFICATION_PASSWORD_ELEM)
        verification_password_elem.send_keys(self.secure_payment_code)
        submit_btn_elem = self.driver.find_element(*self.SUBMIT_BTN_ELEM)
        submit_btn_elem.click()
        sleep(15)



    def get_payment_status_result(self):
        """Returns payment status on payment result page."""
        return self.driver.find_element(*self.PAYMENT_RESULT_ELEM).text
