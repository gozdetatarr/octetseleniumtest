from random import randint

class BasePage:


    def __init__(self, driver):
        self.driver = driver


    def get_title(self):
        return self.driver.title


    def open(self, url):
        self.driver.get(url)

    def get_screenshot(self, path, operation):
        #TODO: Belki saat ekle.
        img_path = path + "/" + operation +  str(randint(1,9999)) + ".png"
        self.driver.save_screenshot(img_path)
