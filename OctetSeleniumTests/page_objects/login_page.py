from selenium.webdriver.common.by import By

from page_objects.base_page import BasePage
from page_objects.home_page import HomePage

from time import sleep
from utils.logger import console_log



class LoginPage(BasePage):
    #Page Elements
    USERNAME_INPUT_ELEM = (By.ID, "ctl00_MainContent_UserName")
    PASSWORD_INPUT_ELEM = (By.ID, "ctl00_MainContent_Password")
    LOGIN_BTN_ELEM = (By.ID, "ctl00_MainContent_LoginButton")
    VERIFICATION_INPUT_ELEM = (By.ID, "ctl00_MainContent_PinText")
    CONTINUE_BTN_ELEM = (By.ID, "ctl00_MainContent_Continue")
    
    

    def __init__(self, driver):
        super().__init__(driver)
        self.username = "snas"
        self.password = "Octet123_"
        self.verification_code = "123456"
        



    def login(self):
        "Performs login operations. Returns Home Page object."
        self.submit_login_form()     
        sleep(3)
        self.submit_login_verification()
        return HomePage(self.driver)



    def submit_login_form(self):
        """Enters username and password to login form."""
        self.driver.find_element(*self.USERNAME_INPUT_ELEM).send_keys(self.username)
        self.driver.find_element(*self.PASSWORD_INPUT_ELEM).send_keys(self.password)
        self.get_screenshot("./artifacts/LoginArtifacts", "submitLoginForm")
        self.driver.find_element(*self.LOGIN_BTN_ELEM).click()
        


    def submit_login_verification(self):
        """Enters verification code to verification form."""
        self.driver.find_element(*self.VERIFICATION_INPUT_ELEM).send_keys(self.verification_code)
        self.driver.find_element(*self.CONTINUE_BTN_ELEM).click()
